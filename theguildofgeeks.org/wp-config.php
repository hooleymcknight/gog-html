<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gog_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '0gr3watch#' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3 Vkv;A%0!y>TMS=f<1~)?DlyAo+Z[_LWKA6pmI@X6V[p7.gb&*9^c6@sK35t7FY');
define('SECURE_AUTH_KEY',  '~-30i2)nAK6l;~6MUYFBkmXQoMkW9s1J_f,WCb. jPj~v4R}WVC-,C-NCh.x%W;_');
define('LOGGED_IN_KEY',    '4<JVt5m>ROY_sk`v@_sBqTBJP<S`}JVpL]79i$-|Lw5*|bs T27wgEB1-epkE,|E');
define('NONCE_KEY',        'J6c?uT2;7,}NKwfkB=SgNn-:PZ;|N7-y,; vRE1&6guQPUis2ki0<}!`@DV(w:z|');
define('AUTH_SALT',        'Rmdij-<Klqqm`i% 3Kr7Sg%s=~1D<|fC:YSqs8CG&|dS4:|uc[P+_XvYXb0?{JSU');
define('SECURE_AUTH_SALT', 'jJ_{qG:t:?M(VLbFO|6e{,DNUvkKt0V*x /U_V|Wy+HuyV,mip[&..v$SD(O<g,k');
define('LOGGED_IN_SALT',   'RU16_2kVf;|g6v}zet<CGW!27m:V{*-e_.@$-xJ!A-:V?wTbZv>u@/ZuTbZL]vie');
define('NONCE_SALT',       'qW>?<F9|U|2[<$2Q;1<YezvHgp><t:Nrp+/@)o4bZ/}K]P?4K+3qLNe>! MO3RzK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'FS_METHOD', 'direct');
define( 'WP_ALLOW_REPAIR', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
