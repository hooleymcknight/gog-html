		<?php cloux_footer(); ?>
		<?php cloux_wrapper_after(); ?>
		<?php wp_footer(); ?>
		<script type="text/javascript">
			(function() {
				jQuery('.back-to-top').hide();
				
				jQuery(window).scroll(function() {
					if(jQuery(window).scrollTop() > 1200) {
						jQuery('.back-to-top').show();
					}
					else {
						jQuery('.back-to-top').hide();
					}
				})
			})();
		</script>
		<script type="text/javascript">
			(function scrollToTop() {
				jQuery('#back-to-top').on('click', function() {
					window.scrollTo(0, 0);
				})
			})();
		</script>
	</body>
</html>