<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:image" content="https://theguildofgeeks.org/wp-content/uploads/2020/01/GoG_weblogo_sq.png" />
		<meta property="og:title" content="The Guild of Geeks" />
		<meta property="og:site_name" content="The Guild of Geeks" />
		<meta property="og:url" content="https://theguildofgeeks.org/" />
		<meta property="og:description" content="Building community through gaming." />
		<meta property="og:type" content="website" />
		
		<link rel="shortcut icon" type="image/x-icon" href="https://theguildofgeeks.org/wp-content/uploads/2020/01/GoG_weblogo_sq.png">
		
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="@guildofgeeks_tx">
		<meta name="twitter:creater" content="@guildofgeeks_tx">
		<meta name="twitter:title" content="Guild of Geeks">
		<meta name="twitter:description" content="Building community through gaming.">
		<meta name="twitter:image" content="https://theguildofgeeks.org/wp-content/uploads/2020/01/GoG_weblogo_sq.png">
		
		<link rel="profile" href="gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<?php cloux_loader(); ?>
		<?php cloux_mobile_header(); ?>
		<?php cloux_header(); ?>
		<?php cloux_wrapper_before(); ?>