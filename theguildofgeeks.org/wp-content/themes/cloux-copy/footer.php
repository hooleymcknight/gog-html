		<?php cloux_footer(); ?>
		<?php cloux_wrapper_after(); ?>
		<?php wp_footer(); ?>
		<script type="text/javascript">
			(function() {
				jQuery('.back-to-top').hide();
				
				jQuery(window).scroll(function() {
					if(jQuery(window).scrollTop() > 1200) {
						jQuery('.back-to-top').show();
					}
					else {
						jQuery('.back-to-top').hide();
					}
				})
			})();
			
			(function scrollToTop() {
				let $ = jQuery;
				jQuery('#back-to-top').on('click', function() {
					window.scrollTo(0, 0);
				});
			})();
		</script>
		<script type="text/javascript">
			(function moveGalleryTabs() {
				let $ = jQuery;
				if(screen.width < 768) {
					let ptab = $('.vc_tta-panel-heading:contains("Photos")');
					let vtab = $('.vc_tta-panel-heading:contains("Videos")');
					$(ptab).addClass('active-tab');
					
					$('.vc_tta-panels').prepend(ptab);
					$(ptab).after($(vtab));
				}
				$('.vc_tta-panels:not(.active-tab)').on('click', function() {
					$('.vc_tta-panel-heading').toggleClass('active-tab');
				});
			})();
		</script>
        <script type="text/javascript">
    		(function addHomeEvents() {
				if(jQuery('#home-events').length > 0) {
					jQuery('#home-events').ready(function() {
            			jQuery('#home-events').append(jQuery('.tribe-events-list-widget'));
        			});
				}
    		})();
        </script>
		<script type="text/javascript">
			(function hideSearchDates() {
				if(jQuery('body.search-results').length > 0) {
					jQuery('.post a[href*="/event/"]').each(function() {
						jQuery(this).parent().parent().addClass('event-search-result');
					});
				}
			})();
		</script>
	</body>
</html>